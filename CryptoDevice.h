#pragma once
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>
#include <stdint.h>
#include <md5.h>
#include <hex.h>

typedef uint8_t byte;
using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);

};
