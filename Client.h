#pragma once

#include "CryptoDevice.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <thread>
#include <map>
#include <tuple>

#pragma comment (lib, "Ws2_32.lib")
#define DEFAULT_BUFLEN 512            
#define IP_ADDRESS "127.0.0.1"
#define DEFAULT_PORT "8202"

std::map<string, tuple<string, string>> users
{ 
	{"Liel", std::make_tuple("BE8220A2528FA6728471709B94A811A2", "E5k8po1MS")}, 
	{"Sagi", std::make_tuple("DACE2797A2ECFBD49A133C7A4DDB9AC2", "784csad4E")}, 
	{"Ron", std::make_tuple("D71CB245275AC66E9A2D6BDF1BF43B37", "4Af5r5OPO")} 
};
// client X - username, password
//client 1 - Liel, coooool
//client 2 - Sagi, what
//client 3 - Ron, lolololol
//the other value in the tuple is the salt

struct client_type
{
	SOCKET socket;
	int id;
	char received_message[DEFAULT_BUFLEN];
};

class Client
{
public:
	Client();
	~Client();
	int main();

private:
	int process_client(client_type  &new_client);
	bool checkClient(string, string);
};

